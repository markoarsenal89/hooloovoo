import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import TableCenter from './TableCenter/TableCenter'
import Player from '../Player/Player'
import { connect } from 'react-redux'
import { drawCards } from '../../actions'
import './Table.scss'

class Table extends PureComponent {
  /**
   * Call action drawCards with number of players as a parameter
  */
  drawCards = (e) => {
    e.target.style.display = "none"
    this.props.drawCards(this.props.numberOfPlayers)
  }

  render() {
    return (
      <div className={`table players-${this.props.numberOfPlayers}`}>
        <div className="center">
          {this.props.cards.length === 0 ?
            <button type="button" className="btn draw-btn" onClick={this.drawCards}>Draw the cards</button>
            : <TableCenter />
          }
        </div>
        {this.props.players.map((player, i) => {
          return <Player player={player} cards={this.props.cards[i]} key={player.id} />
        })}
      </div>
    )
  }
}

Table.defaultProps = {
  numberOfPlayers: 0,
  drawCards: () => {},
  cards: [],
}

Table.propsTypes = {
  numberOfPlayers: PropTypes.number,
  drawCards: PropTypes.func,
  cards: PropTypes.array,
}

function mapStateToProps (state) {
  return {
    players: state.players,
    numberOfPlayers: state.game.numberOfPlayers,
    cards: state.cards,
  }
}

export default connect(mapStateToProps, { drawCards })(Table)
