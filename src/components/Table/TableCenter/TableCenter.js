import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { stopRound, clearPlayedCards, updateScore } from '../../../actions'
import { cardsValue } from '../../../config'
import Card from '../../Card/Card'
import confetti from '../../../assets/images/confetti.gif'
import './TableCenter.scss'

class TableCenter extends PureComponent {
  constructor() {
    super()
    this.state = { gameWinners: [] }
  }

  componentDidUpdate() {
    // End round and set new score
    if (this.props.playedCards.length === this.props.numberOfPlayers) {
      setTimeout(() => {
        this.props.stopRound()
        this.props.clearPlayedCards()
        this.props.updateScore(this.winner)

        if (this.props.gameFinished) {
          this.setGameWinner()
        }
      }, 2000)
    }
  }

  /**
   * Set game winner at the end of the game
   */
  setGameWinner = () => {
    const players = this.props.players
    let winners = [{ score: 0 }]

    for (let i in players) {
      if (players[i].score > winners[0].score) {
        winners = [players[i]]
      } else if (players[i].score === winners[0].score) {
        winners.push(players[i])
      }
    }

    this.setState({ gameWinners: winners })
  }

  /**
   * Calculate round winner
   * @param { Array } playedCards Array of played cards
   * @returns { Object } Object with data about card and player
   */
  roundWinner = (playedCards) => {
    let winner = {
      cardValue: 0,
      player: {},
      score: 0,
    }

    for (let i in playedCards) {
      const card = playedCards[i].card
      const cardValue = isNaN(parseInt(card.value)) ? cardsValue[card.value] : parseInt(card.value)

      if (cardValue >= winner.cardValue) {
        winner.cardValue = cardValue
        winner.player = playedCards[i].player
      }

      winner.score += cardValue
    }

    return winner
  }

  renderPlayedCards = () => {
    // Render when all cards are ready
    if (this.props.numberOfPlayers !== this.props.playedCards.length) {
      return null
    }

    this.winner = this.roundWinner(this.props.playedCards)

    return this.props.playedCards.map((data, i) => {
      const winnerClass = data.player.id === this.winner.player.id ? 'winner' : ''

      return <div className={`played-card player-${data.player.type} ${winnerClass}`} key={i}>
        <h4 className="player-name">{data.player.name}</h4>
        <Card card={data.card} showCardBack={false} />
      </div>
    })
  }

  renderGameWinner = () => {
    return <div className="game-winner">
      {this.state.gameWinners.map((winner, i) => {
        const winnerName = winner.name === 'Me' ? 'You' : winner.name
        return <h4 className="winner-name" key={i}>{winnerName} won!!!</h4>
      })}
      <img src={confetti} alt="confetti" className="confetti" />
    </div>
  }

  render() {
    const gameWinnerClass = this.state.gameWinners.length ? 'game-winner' : ''

    return <div className={`table-center ${gameWinnerClass}`}>
      {this.state.gameWinners.length ? this.renderGameWinner() : this.renderPlayedCards()}
    </div>
  }
}

TableCenter.defaultProps = {
  playedCards: [],
}

TableCenter.propTypes = {
  playedCards: PropTypes.array,
}

function mapStateToProps (state) {
  return {
    gameFinished: state.game.finished,
    players: state.players,
    numberOfPlayers: state.game.numberOfPlayers,
    playedCards: state.playedCards,
  }
}

export default connect(mapStateToProps, {
  stopRound,
  clearPlayedCards,
  updateScore,
})(TableCenter)
