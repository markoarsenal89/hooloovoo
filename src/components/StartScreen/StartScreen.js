import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { startTheGame } from '../../actions'
import './StartScreen.scss'

const aiPlayers = ['Ola', 'Rosetta', 'Carolyn']

class StartScreen extends PureComponent {
  startTheGame = (numberOfPlayers) => {
    const players = aiPlayers.slice(0, numberOfPlayers - 1).map((name, i) => {
      return {
        id: i,
        name,
        type: 'ai',
        order: i,
        score: 0,
      }
    })

    players.push({
      id: numberOfPlayers - 1,
      name: 'Me',
      type: 'human',
      order: numberOfPlayers - 1,
      score: 0,
    })

    this.props.startTheGame(players, players.length)
  }

  render() {
    return (
      <div className="start-screen">
        <h2 className="heading">Select number of players</h2>
        <button type="button" className="btn" onClick={() => { this.startTheGame(2) }}>2 players</button>
        <button type="button" className="btn" onClick={() => { this.startTheGame(3) }}>3 players</button>
        <button type="button" className="btn" onClick={() => { this.startTheGame(4) }}>4 players</button>
      </div>
      )
    }
  }

export default connect(null, { startTheGame })(StartScreen)
