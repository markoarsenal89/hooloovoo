import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { startRound, playCard } from '../../actions'
import Card from '../Card/Card'
import './Player.scss'

class Player extends PureComponent {
  constructor() {
    super()
    this.activeCard = -1
  }

  componentDidUpdate(prevProps) {
    // Play a card if isRoundStarted flag is set to true
    if (prevProps.isRoundStarted === false && this.props.isRoundStarted === true) {
      if (this.props.player.type === 'human') {
        const card = this.props.cards[this.activeCard]
        this.props.playCard(card, this.activeCard, this.props.player)
      } else {
        const cardIndex = this.getRandomCardIndex(this.props.cards)
        const aiCard = this.props.cards[cardIndex]

        this.props.playCard(aiCard, cardIndex, this.props.player)
      }
    }
  }

  /**
   * Generate random index for cards array
   * @param { Array } cards Array of cards
   */
  getRandomCardIndex = (cards) => {
    return Math.floor(Math.random() * cards.length)
  }

  /**
   * Start a round with clicking on a card,
   * and changing the flag in the global state
   * Round can start only 'human' player
   * @param { Object } i Card index
   */
  startRound = (i) => {
    if (this.props.player.type === 'ai') {
      return
    } else {
      this.props.startRound()
      this.activeCard = i
    }
  }

  render() {
    return (
      <div className={`player player-${this.props.player.type} player-${this.props.player.order + 1}`}>
        <div className="player-container">
          <h4 className="player-name">{this.props.player.name}</h4>
          <p className="score">Score: <strong>{this.props.player.score}</strong></p>
          <div className="cards">
            {this.props.cards.map((card, i) => {
              return <Card
                card={card}
                showCardBack={this.props.player.type === 'ai' ? true : false}
                onPlayCard={this.startRound}
                order={i}
                key={card.code} />
            })}
          </div>
        </div>
      </div>
    )
  }
}

Player.defaultProps = {
  player: {
    id: 0,
    name: 'No name :(',
    type: 'ai',
    order: 0,
  },
  cards: [],
  startRound: () => {},
  playCard: (card, cardIndex, playerIndex) => {},
}

Player.propTypes = {
  player: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    type: PropTypes.string,
    order: PropTypes.number,
  }),
  cards: PropTypes.array,
  startRound: PropTypes.func,
  playCard: PropTypes.func,
}

function mapStateToProps (state) {
  return { isRoundStarted: state.isRoundStarted }
}

export default connect(mapStateToProps, {
  startRound,
  playCard,
})(Player)
