import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { finishTheGame } from '../../actions'
import StartScreen from '../StartScreen/StartScreen'
import Table from '../Table/Table'
import './Game.scss'

class Game extends PureComponent {
  componentDidUpdate(pp) {
    const finished = this.props.game.finished
    const cards = this.props.cards[0]
    const pCards = this.props.playedCards

    // Finish the game if all cards are played
    if (cards && cards.length === 0 && pCards.length === 0 && !finished) {
      this.props.finishTheGame()
    }
  }

  render() {
    return (
      <div className="game">
        {this.props.game.started === false ? <StartScreen /> : <Table />}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    game: state.game,
    cards: state.cards,
    playedCards: state.playedCards,
  }
}

export default connect(mapStateToProps, { finishTheGame })(Game)
