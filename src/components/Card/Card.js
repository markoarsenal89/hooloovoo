import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import cardBackImage from './card-back.jpg'
import './Card.scss'

class Card extends PureComponent {
  render() {
    const cardImage = this.props.showCardBack ? cardBackImage : this.props.card.image

    return (
      <div
        className="card"
        style={{ backgroundImage: `url(${cardImage})` }}
        onClick={() => {
          this.props.onPlayCard(this.props.order)
        }}></div>
    )
  }
}

Card.defaultProps = {
  card: PropTypes.shape({
    image: PropTypes.string,
    value: PropTypes.number,
  }),
  showCardBack: PropTypes.bool,
  order: PropTypes.number,
  onPlayCard: PropTypes.func,
}

Card.propTypes = {
  card: {},
  showCardBack: false,
  order: 0,
  onPlayCard: (cardIndex) => {},
}

export default Card
