export const START_THE_GAME = 'START_THE_GAME'
export const FINISH_THE_GAME = 'FINISH_THE_GAME'
export const DRAW_CARDS = 'DRAW_CARDS'
export const START_ROUND = 'START_ROUND'
export const STOP_ROUND = 'STOP_ROUND'
export const PLAY_CARD = 'PLAY_CARD'
export const CLEAR_PLAYED_CARDS = 'CLEAR_PLAYED_CARDS'
export const UPDATE_SCORE = 'UPDATE_SCORE'

const deckId = 'dzfgz21c1gvt'
const shuffleDeckUrl = `https://deckofcardsapi.com/api/deck/${deckId}/shuffle/`
const drawCardsUrl = `https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=10`

export function startTheGame (players, numberOfPlayers) {
  return {
    type: START_THE_GAME,
    players,
    numberOfPlayers,
  }
}

export function finishTheGame () {
  return { type: FINISH_THE_GAME }
}

function shuffleDeck () {
  return fetch(shuffleDeckUrl)
}

function fetchCards () {
  return fetch(drawCardsUrl).then(function (response) {
    return response.json()
  }).then((data) => {
    return data.cards
  })
}

export function drawCards (numberOfPlayers) {
  return (dispatch) => {
    shuffleDeck().then(() => {
      const promises = []

      for (let i = 0; i < numberOfPlayers; i++) {
        promises.push(fetchCards())
      }

      Promise.all(promises).then((cards) => {
        dispatch({
          type: DRAW_CARDS,
          cards
        })
      })
    })
  }
}

export function startRound () {
  return { type: START_ROUND }
}

export function stopRound () {
  return { type: STOP_ROUND }
}

export function playCard (card, cardIndex, player) {
  return {
    type: PLAY_CARD,
    card,
    cardIndex,
    player,
  }
}

export function clearPlayedCards () {
  return { type: CLEAR_PLAYED_CARDS }
}

export function updateScore (winner) {
  return {
    type: UPDATE_SCORE,
    winner,
  }
}
