import { combineReducers } from 'redux'
import {
  START_THE_GAME,
  FINISH_THE_GAME,
  DRAW_CARDS,
  START_ROUND,
  STOP_ROUND,
  PLAY_CARD,
  CLEAR_PLAYED_CARDS,
  UPDATE_SCORE,
} from './actions'

const gameInitialState = {
  started: false,
  finished: false,
  numberOfPlayers: 0,
}

function game (state = gameInitialState, action) {
  switch (action.type) {
    case START_THE_GAME:
      return {
        ...state,
        started: true,
        numberOfPlayers: action.numberOfPlayers,
      }
    case FINISH_THE_GAME:
      return {
        ...state,
        finished: true,
      }
    default:
      return state
  }
}

function players (state = [], action) {
  switch (action.type) {
    case START_THE_GAME:
      return action.players
    case UPDATE_SCORE:
      const newState = [...state]

      for (let i in newState) {
        if (newState[i].id === action.winner.player.id) {
          newState[i] = {...action.winner.player}
          newState[i].score += action.winner.score
        }
      }

      return newState
    default:
      return state
  }
}

function cards (state = [], action) {
  switch (action.type) {
    case DRAW_CARDS:
      return action.cards
    case PLAY_CARD:
      const newState = [...state]

      newState[action.player.order] = [...newState[action.player.order]]
      newState[action.player.order].splice(action.cardIndex, 1)

      return newState
    default:
      return state
  }
}

function isRoundStarted (state = false, action) {
  switch (action.type) {
    case START_ROUND:
      return true
    case STOP_ROUND:
      return false
    default:
      return state
  }
}

function playedCards (state = [], action) {
  switch (action.type) {
    case PLAY_CARD:
      const playedCard = {
        card: action.card,
        player: action.player,
      }
      return [...state, playedCard]
    case CLEAR_PLAYED_CARDS:
      return []
    default:
      return state
  }
}

export default combineReducers({
  game,
  players,
  cards,
  isRoundStarted,
  playedCards,
})