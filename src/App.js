import React, { Component } from 'react'
import Game from './components/Game/Game'
import './assets/css/style.scss'

class App extends Component {
  render() {
    return (
      <div className="app">
        <Game />
      </div>
    )
  }
}

export default App
